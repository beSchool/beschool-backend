import BeSchool from '../beschool/BeSchool';
import BescoveryRepositoryInterface from '../repositories/BescoveryRepository';
import ImageRepositoryInterface from '../repositories/ImageRepository';

const express = require('express');
const mongoose = require("mongoose");

export default (bescoveryRepository: BescoveryRepositoryInterface, imageRepository: ImageRepositoryInterface) => {
    const router = express.Router();
    let beSchool = new BeSchool(bescoveryRepository, imageRepository);
    const upload = beSchool.getUploadMiddleware();

    router.post('/upload', upload.single('file'), (req: any, res: any, next: any) => {
        return res.status(200).send(req.file.url)
    })

    router.get('/planets', (req: any, res: any) => {
        beSchool.fetchPlanets().then((planets: any) => {
            return res.json(planets)
        })
    })

    router.post('/planets', (req: any, res: any) => {
        try {
            bescoveryRepository.createPlanet(req.body.name);
            return res.status(204).send()
        } catch (e) {
            return res.status(422).send();
        }
    });

    router.get('/planets/:id', (req: any, res: any) => {
        beSchool.fetchPlanet(req.params.id)
            .then((planet) => { res.json(planet) })
            .catch((e) => { res.status(422).send();})
    });

    router.get('/planets/:id/bescoveries', (req: any, res: any) => {
        beSchool.fetchBescoveriesFromPlanet(req.params.id).then((bs) => {
            res.json(bs)
        }).catch(e => {
            res.status(422).send();
        })
    });

    router.get('/planets/:id/bescoveries/:bsId', (req: any, res: any) => {
        beSchool.fetchBescoveryFromPlanet(req.params.id, req.params.bsId).then((bs) => {
            res.json(bs)
        }).catch(e => {
            res.status(422).send();
        })
    });

    router.post('/admin/', (req: any, res: any) => {
        beSchool.isAuthorize(req.body.email)
            .then((isAuthorize: boolean) => res.json({ is_authorize: isAuthorize }))
            .catch((e: any) => {
                res.status(422).send()
            });
    });

    router.post('/planets/:id/bescoveries', (req: any, res: any) => {
        const { body = {} } = req
        const bescoveryData = {
            title: body.title,
            difficulty: +body.difficulty,
            departure_date: new Date(body.departure_date),
            video_url: body.video_url,
            gallery: body.gallery,
            presentation: body.presentation,
            summary: body.summary,
            pilots: body.pilots
        }
        beSchool.addBescoveryToPlanet(req.params.id, bescoveryData)
            .then(() => res.status(204).send())
            .catch((e) => {
                res.status(422).send()
            });
    })

    router.put('/planets/:id/bescoveries/:bsId', (req: any, res: any) => {
        const { body = {} } = req
        const bescoveryData = {
            title: body.title,
            difficulty: +body.difficulty,
            departure_date: new Date(body.departure_date),
            video_url: body.video_url,
            gallery: body.gallery,
            presentation: body.presentation,
            summary: body.summary,
            type: body.type,
            pilots: body.pilots
        }
        beSchool.updateBescoveryFromPlanet(req.params.id, req.params.bsId, bescoveryData)
            .then((bs) => res.json(bs))
            .catch((e) => {
                res.status(422).send()
            });
    })

    router.delete('/planets/:id/bescoveries/:bsId', (req: any, res: any) => {
        beSchool.deleteBescoveryFromPlanet(req.params.id, req.params.bsId)
            .then((bs) => res.json(bs))
            .catch((e: any) => res.status(422).send() )
    });

    return router
}
