'use strict';

import BescoveryRepositoryInterface from "./repositories/BescoveryRepository";
import routes from "./routes";
import ImageRepositoryInterface from "./repositories/ImageRepository";
import InMemoryImageRepository from "../test/InMemoryImageRepository";

const express = require('express')
const bodyParser = require('body-parser')
const path = require("path");

export default (
  bescoveryRepository: BescoveryRepositoryInterface,
  imageRepository: ImageRepositoryInterface = new InMemoryImageRepository(),
  filesPath?: string,
) => {
  const app = express();
  app.use("/uploads", express.static(path.join(__dirname, "../../uploads/")));

  if (filesPath)
    app.use(express.static(filesPath));

  app.use(function (req: any, res: any, next: any) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header(
      "Access-Control-Allow-Headers",
      "Origin, X-Requested-With, Content-Type, Accept",
    );
    next();
  });
  app.use(bodyParser.json());
  app.use(routes(bescoveryRepository, imageRepository));

  return app;
};
