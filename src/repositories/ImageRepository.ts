interface ImageRepository {
    getUploadMiddleware(): any;

    getUploadPath(): String;
}

export default ImageRepository;
