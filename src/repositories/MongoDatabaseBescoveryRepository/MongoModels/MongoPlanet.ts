const mongoose = require('mongoose')
const Schema = mongoose.Schema

const PlanetSchema = new Schema({
  name: {
    type: String,
    required: true
  }
})

const MongoPlanet = mongoose.model('Planet', PlanetSchema)
export default MongoPlanet

