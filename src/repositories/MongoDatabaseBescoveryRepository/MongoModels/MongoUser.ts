const mongoose = require('mongoose')
const Schema = mongoose.Schema

const UserSchema = new Schema({
  email: {
    type: String,
    required: true
  },
  is_admin: {
      type: Boolean,
      required: true
    }
})

const MongoUser = mongoose.model('User', UserSchema)
export default MongoUser
