const mongoose = require('mongoose')
const Schema = mongoose.Schema

const BescoverySchema = new Schema({
  title: {
    type: String,
    required: true
  },
  planet: {
    type: String,
    required: true
  },
  pilots: {
    type: Array,
    required: false,
    default: []
  },
  type: {
    type: String,
    required: false,
    default: '0'
  },
  difficulty: {
    type: Number,
    default: 0,
    min: 0,
    max: 10
  },
  video_url: {
    type: String,
    required: false,
    default: null,
  },
  presentation: {
    type: String,
    required: false,
    default: null
  },
  gallery: {
    type: Array,
    required: false,
    default: []
  },
  departure_date: {
    type: Date,
    required: true
  },
  summary: {
    type: String,
    required: true
  }
})

const MongoBescovery = mongoose.model('Bescovery', BescoverySchema)
export default MongoBescovery
