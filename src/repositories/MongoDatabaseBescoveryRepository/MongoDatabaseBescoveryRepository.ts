'use strict';

import BescoveryRepository from '../BescoveryRepository';
import Bescovery from "../../beschool/bescovery/Bescovery";
import Planet from "../../beschool/planet/Planet";
import User from "../../beschool/bescovery/User";
import MongoPlanet from "./MongoModels/MongoPlanet";
import MongoBescovery from "./MongoModels/MongoBescovery";
import MongoUser from "./MongoModels/MongoUser";

class MongoDatabaseBescoveryRepository implements BescoveryRepository {
    private readonly bescoveries: Bescovery[];
    private readonly planets: Planet[];

    constructor() {
        this.planets = []
        this.bescoveries = []
    }

    createPlanet(name: string) {
        const planet = new MongoPlanet({ name })
        return planet.save()
    }

    addBescoveryToPlanet(planetId: number, bescovery: Bescovery) {
        const bs = new MongoBescovery(bescovery)
        return bs.save()
    }

    getAllBescoveries(): Bescovery[] {
        return this.bescoveries;
    }

    async getAllBescoveriesFromPlanet(planetId: string): Promise<Bescovery[]> {
         try {
            const planet = await MongoPlanet.findById(planetId)
            const bs = await MongoBescovery.find({ planet: planetId })
            return bs.map((raw: any) => {
                return new Bescovery(
                    raw._id || raw.id,
                    raw.title,
                    planet,
                    raw.pilots,
                    raw.type,
                    raw.difficulty,
                    raw.departure_date,
                    raw.video_url,
                    raw.presentation,
                    raw.gallery,
                    raw.summary);
            });
        } catch (e) {
            throw new Error(e)
        }
    }

    async getOneBescoveryFromPlanet(planetId: string, bescoveryId: string): Promise<Bescovery> {
        try {
            const planet = await MongoPlanet.findById(planetId)
            const bs = await MongoBescovery.findOne({ planet: planetId, _id: bescoveryId })
            return new Bescovery(
                bs._id || bs.id,
                bs.title,
                planet,
                bs.pilots,
                bs.type,
                bs.difficulty,
                bs.departure_date || bs.departureDate,
                bs.video_url,
                bs.presentation,
                bs.gallery,
                bs.summary
            );
        } catch (e) {
            throw new Error(e)
        }
    }

    async getAllPlanets(): Promise<Planet[]> {
        try {
            const planets = await MongoPlanet.find({})
            return planets.map(((raw: any) => {
               return new Planet(raw._id, raw.name)
           }))
        } catch (e) {
            throw new Error(e)
        }
    }

    async getPlanet(id: string): Promise<Planet> {
        try {
           const rawPlanet= await MongoPlanet.findById(id)
           return new Planet(rawPlanet._id, rawPlanet.name)
        } catch (e) {
            throw new Error(e)
        }
    }

    createBescovery (bescoveryData: object): Promise<any> {
        try {
            const bs = new MongoBescovery(bescoveryData)
            return bs.save()
        } catch (e) {
            throw new Error(e)
        }
    }

    async updateBescovery(planetId: string, bescoveryId: string, bsData: object): Promise<Bescovery> {
        let bs = undefined;
        try {
            bs = await MongoBescovery.findOneAndUpdate({planet: planetId, _id: bescoveryId}, bsData, {
                new: true,
                runValidators: true
            })
        } catch (e) {
            throw new Error(e)
        }

        if (bs == undefined) {
            throw new Error("not found")
        }

        return new Bescovery(
            bs._id || bs.id,
            bs.title,
            new Planet(planetId, bs.planet.name),
            bs.pilots,
            bs.type,
            bs.difficulty,
            bs.departure_date || bs.departureDate,
            bs.video_url,
            bs.presentation,
            bs.gallery,
            bs.summary
        );
    }

    async deleteBescoveryFromPlanet(planetId: string, bsId: string): Promise<Bescovery> {
        let bs = undefined;
        try {
            bs = await MongoBescovery.findOneAndRemove({planet: planetId, _id: bsId})
        } catch (e) {
            throw new Error(e)
        }

        if (bs == undefined) {
            throw new Error("bescovery not found")
        }

        return new Bescovery(
            bs._id || bs.id,
            bs.title,
            new Planet(planetId, bs.planet.name),
            bs.pilots,
            bs.type,
            bs.difficulty,
            bs.departure_date || bs.departureDate,
            bs.video_url,
            bs.presentation,
            bs.gallery,
            bs.summary
        );
    }

    async isAuthorize(user: User): Promise<boolean> {
        try {
            const countUser = await MongoUser.countDocuments({ email: user.email, is_admin: true})
            return countUser >= 1
        } catch (e) {
            throw new Error(e)
        }
    }
}

export default MongoDatabaseBescoveryRepository
