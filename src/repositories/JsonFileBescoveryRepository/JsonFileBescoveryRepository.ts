'use strict';

import BescoveryRepository from '../BescoveryRepository';
import Bescovery from "../../beschool/bescovery/Bescovery";
import Planet from "../../beschool/planet/Planet";
import User from "../../beschool/bescovery/User";

const raw_planets = require('../../../../data/planets.json');
const raw_bescoveries = require('../../../../data/bescoveries.json');

class JsonFileBescoveryRepository implements BescoveryRepository {
    private readonly bescoveries: Bescovery[];
    private readonly planets: Planet[];

    constructor() {
        this.planets = this.readPlanets();
        this.bescoveries = this.readBescoveries();
    }

    private readPlanets() {
        return raw_planets.map((planet: JsonFilePlanet) => new Planet(planet.id, planet.name));
    }

    private readBescoveries() {
        return raw_bescoveries.map((bs: JsonFileBescovery) =>
            new Bescovery(
                bs.id, bs.title, new Planet(bs.planet, ''),
                bs.pilots, bs.type, bs.difficulty, bs.departure_date,
                bs.video_url, bs.presentation, bs.gallery, bs.summary));
    }

    getAllBescoveries(): Bescovery[] {
        return this.bescoveries;
    }

    getAllBescoveriesFromPlanet(planetId: string): Promise<Bescovery[]> {
        return new Promise((resolve, reject) => {
            const bs = this.bescoveries.filter(b => b.isFrom(planetId));
            if (!bs) reject(new Error(`No bescoveries for Planet ${planetId}`))
            resolve(bs)
        })
    }

    getOneBescoveryFromPlanet(planetId: string, bescoveryId: string): Promise<Bescovery> {
        return new Promise((resolve, reject) => {
            const bs = this.bescoveries.filter(b => b.isFrom(planetId) && b.hasId(bescoveryId))
            if (!bs || !bs.length) reject(new Error('BS not found'))
            resolve(bs[0])
        })
    }

    getAllPlanets(): Promise<Planet[]> {
        return Promise.resolve(this.planets);
    }

    getPlanet(id: string): Promise<Planet> {
        return new Promise((resolve, reject) => {
            const planet = this.planets.filter(p => p.id === parseInt(id));
            if (!planet || !planet.length) reject(new Error(`Planet ${id} not found`))
            resolve(planet[0])
        })
    }

    createPlanet(name: string): void {}
    createBescovery(data: object): Promise<any> { return Promise.resolve() }
    updateBescovery(planetId: string, bescoveryId: string, bsData: object): Promise<any> { return Promise.resolve() }
    deleteBescoveryFromPlanet(planetId: string, bsId: string): Promise<any> { return Promise.resolve() }

    isAuthorize(user: User): Promise<boolean> { return Promise.resolve(false)}
}

export default JsonFileBescoveryRepository
