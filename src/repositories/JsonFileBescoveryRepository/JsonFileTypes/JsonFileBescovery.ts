type JsonFileBescovery = {
    id: number,
    title: string,
    planet: number,
    pilots: string[],
    type: number,
    difficulty: number,
    departure_date: string,
    video_url?: string,
    presentation?: string,
    gallery?: string[],
    summary?: string
};