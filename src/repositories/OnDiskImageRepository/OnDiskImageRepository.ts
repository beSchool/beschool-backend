import ImageRepository from "../ImageRepository";

const multerCloudinaryStorage = require('multer-storage-cloudinary');
const cloudinary = require('cloudinary');
const multer = require("multer");

const AUTHORIZED_IMAGE_EXTENSION = ['jpg','jpeg','png','gif'];

class OnDiskImageRepository implements ImageRepository {
    private readonly uploadPath: String;
    private readonly diskStorageCloudName: string;
    private readonly diskStorageApiKey: string;
    private readonly diskStorageApiSecret: string;


    constructor(uploadPath: String, diskStorageCloudName: string, diskStorageApiKey: string, diskStorageApiSecret: string) {
        this.uploadPath = uploadPath;
        this.diskStorageCloudName = diskStorageCloudName;
        this.diskStorageApiKey = diskStorageApiKey;
        this.diskStorageApiSecret = diskStorageApiSecret;
    }

    getUploadPath(): String {
        return this.uploadPath;
    }

    getFileFilter(): any {
        return function (req: any, file: any, cb: any) {
            if (!new TextUtils().endsWith(AUTHORIZED_IMAGE_EXTENSION, file.originalname)) {
                return cb(new Error('Only image files are allowed!'), false);
            }
            cb(null, true);
        }
    }

    getUploadMiddleware(): any {
        cloudinary.config({
            cloud_name: this.diskStorageCloudName,
            api_key: this.diskStorageApiKey,
            api_secret: this.diskStorageApiSecret,
        });

        const storage = multerCloudinaryStorage({
            cloudinary: cloudinary,
            folder: this.uploadPath,
            allowedFormats: AUTHORIZED_IMAGE_EXTENSION,
            filename: function (req: any, file: any, cb: any) {
                cb(undefined, '');
            }
        });
        const fileSizeLimit = 4 * 1024 * 1024;

        return multer({ storage: storage, fileFilter: this.getFileFilter(), limits: { fileSize: fileSizeLimit } })
    }
}

class TextUtils {
    endsWith(suffixes: string[], text: String) {
        for (let suffix of suffixes) {
            if(text.endsWith(suffix)) {
                return true;
            }
        }
        return false;
    }
}

export default OnDiskImageRepository;
