import Bescovery from "../beschool/bescovery/Bescovery";
import Planet from "../beschool/planet/Planet";
import User from "../beschool/bescovery/User";

interface BescoveryRepository {
    getAllBescoveries(): Bescovery[];

    getAllBescoveriesFromPlanet(planetId: string): Promise<Bescovery[]>;

    getOneBescoveryFromPlanet(planetId: string, bescoveryId: string): Promise<Bescovery>;

    getAllPlanets(): Promise<Planet[]>;

    getPlanet(id: string): Promise<Planet>;

    createPlanet(name: string): void;

    createBescovery(bsData: object): Promise<any>;

    updateBescovery(planetId: string, bescoveryId: string, bsData: object): Promise<Bescovery>

    deleteBescoveryFromPlanet(planetId: string, bsId: string): Promise<Bescovery>;

    isAuthorize(user: User): Promise<boolean>;
}

export default BescoveryRepository;