"use strict";

import MongoDatabaseBescoveryRepository from "./repositories/MongoDatabaseBescoveryRepository/MongoDatabaseBescoveryRepository";
import OnDiskImageRepository from "./repositories/OnDiskImageRepository/OnDiskImageRepository";

import server from "./server";
const mongoose = require("mongoose");
const isProd = process.env.NODE_ENV === 'production'
if (!isProd)
    require("dotenv").config();

mongoose.Promise = global.Promise
const port = process.env.PORT || 8080;
const dbUri = isProd ? process.env.DB_URI : process.env.DB_TEST;
const diskStorageCloudName = isProd ? process.env.DISK_STORAGE_CLOUD_NAME : process.env.DISK_STORAGE_CLOUD_NAME_TEST;
const diskStorageApiKey = isProd ? process.env.DISK_STORAGE_API_KEY : process.env.DISK_STORAGE_API_KEY_TEST;
const diskStorageApiSecret = isProd ? process.env.DISK_STORAGE_API_SECRET : process.env.DISK_STORAGE_API_SECRET_TEST;

//const dbUri = 'mongodb://localhost:27017/beschool' // Keep this line for local tests
let app = null

mongoose.connect(dbUri, { useNewUrlParser: true })
    .then(() => {
        if (!diskStorageCloudName || !diskStorageApiKey || !diskStorageApiSecret) {
            console.error('You should define environment variable DISK_STORAGE_CLOUD_NAME, DISK_STORAGE_API_KEY, DISK_STORAGE_API_SECRET. ' +
                'Use BeSchool Cloudinary\'s account');
            return
        }

        const diskStorage = new OnDiskImageRepository('images', diskStorageCloudName, diskStorageApiKey, diskStorageApiSecret)

        app = server(new MongoDatabaseBescoveryRepository(), diskStorage, './data');
        if (!module.parent) {
            app.listen(port, () => {
                console.log(`Listening on ${port}`)
            })
        }
    }).catch((error: any) => console.error(error))

export default app
