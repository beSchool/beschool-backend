class Planet {
    readonly id: any; // could be a string
    readonly name: string;

    constructor(id: any, name: string) {
        this.id = id;
        this.name = name;
    }
}

export default Planet