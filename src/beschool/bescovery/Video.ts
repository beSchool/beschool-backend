type PreviewImages = {
    high_res: string,
    medium_res: string,
    low_res: string;
}

class Video {
    readonly url: string;
    readonly preview_images: PreviewImages;

    static createFromUrl(url: string): Video {
        const videoParameter = "v=";
        const index = url.indexOf(videoParameter);
        const videoIdLength = 11;
        const videoId = url.substr(index + videoParameter.length, videoIdLength);
        return new Video(
            url,
            {
                high_res: `https://img.youtube.com/vi/${videoId}/hqdefault.jpg`,
                medium_res: `https://img.youtube.com/vi/${videoId}/mqdefault.jpg`,
                low_res: `https://img.youtube.com/vi/${videoId}/default.jpg`
            });
    }

    constructor(url: string, previewImage: PreviewImages) {
        this.url = url;
        this.preview_images = previewImage;
    }
}

export default Video;