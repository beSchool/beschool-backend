'use strict';

import Planet from "../planet/Planet";
import BescoveryDTO from "./BescoveryDTO";
import Video from "./Video";
import Image from "./Image";
import BescoveryCharacteristics from "./BescoveryCharacteristics";

class Bescovery {
    private readonly id: number;
    private readonly title: string;
    private readonly planet: Planet;
    private readonly pilots: string[];
    private readonly type: number;
    private readonly difficulty: number;
    private readonly departureDate: string;
    private readonly video?: Video;
    private readonly presentation?: string;
    private readonly gallery?: Image[];
    private readonly summary?: string;

    constructor(id: number, title: string, planet: Planet, pilots: string[], type: number, difficulty: number, departureDate: string, videoUrl?: string, presentation?: string, images?: string[], summary?: string) {
        this.id = id;
        this.title = title;
        this.planet = planet;
        this.pilots = pilots;
        this.type = type;
        this.difficulty = difficulty;
        this.departureDate = departureDate;
        if (videoUrl) {
            this.video = Video.createFromUrl(videoUrl);
        }
        this.presentation = presentation;
        if (images)
            this.gallery = images.map((imageUrl: string) => new Image(imageUrl));
        this.summary = summary;
    }

    characteristics(): BescoveryCharacteristics {
        return {
            id: this.id,
            title: this.title,
            pilots: this.pilots,
            type: this.type,
            difficulty: this.difficulty,
            departure_date: this.departureDate
        };
    }

    full() {
        return new BescoveryDTO(
            this.id, this.title, this.planet.name, this.pilots, this.type,
            this.difficulty, this.departureDate, this.video, this.presentation, this.gallery, this.summary);
    }

    isFrom(planetId: string): boolean {
        return String(this.planet.id) === planetId;
    }

    hasId(id: string): boolean {
        return String(this.id) === String(id);
    }
}

export default Bescovery
