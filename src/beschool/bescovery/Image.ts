class Image {
  readonly high_res: string;
  readonly medium_res: string;

  constructor(imageUrl: string) {
    this.high_res = imageUrl;
    this.medium_res = imageUrl;
  }
}

export default Image;