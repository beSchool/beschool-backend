class User {
  readonly email: string;

  constructor(email: string) {
    this.email = email;
  }
}

export default User;