type BescoveryCharacteristics = {
    id: number,
    title: string,
    pilots: string[],
    type: number,
    difficulty: number,
    departure_date: string
}

export default BescoveryCharacteristics;