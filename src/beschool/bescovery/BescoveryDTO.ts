import Video from "./Video";
import Image from "./Image";

class BescoveryDTO {
    private id: number;
    private title: string;
    private planet: string;
    private pilots: string[];
    private type: number;
    private difficulty: number;
    private departure_date: string;
    private video?: Video;
    private presentation?: string;
    private gallery?: Image[];
    private summary?: string;

    constructor(id: number, title: string, planet: string, pilots: string[], type: number, difficulty: number, departureDate: string, video?: Video, presentation?: string, gallery?: Image[], summary?: string) {
        this.id = id;
        this.title = title;
        this.planet = planet;
        this.pilots = pilots || [];
        this.type = type || 0;
        this.difficulty = difficulty;
        this.departure_date = departureDate;
        this.video = video;
        this.presentation = presentation;
        this.gallery = gallery;
        this.summary = summary;
    }
}

export default BescoveryDTO;