'use strict';

import BescoveryRepository from '../repositories/BescoveryRepository';
import ImageRepository from '../repositories/ImageRepository';
import Bescovery from "./bescovery/Bescovery";
import Planet from "./planet/Planet";
import BescoveryDTO from "./bescovery/BescoveryDTO";
import BescoveryCharacteristics from "./bescovery/BescoveryCharacteristics";
import User from "./bescovery/User";

class BeSchool {
    private bescoveryRepository: BescoveryRepository;
    private imageRepository: ImageRepository;

    constructor(bescoveryRepository: BescoveryRepository, imageRepository: ImageRepository) {
        this.bescoveryRepository = bescoveryRepository;
        this.imageRepository = imageRepository;
    }

    getUploadPath() {
        return this.imageRepository.getUploadPath()
    }

    getUploadMiddleware() {
        return this.imageRepository.getUploadMiddleware();
    }

    fetchPlanets(): Promise<any> {
        return this.bescoveryRepository.getAllPlanets();
    }

    async fetchPlanet(id: string): Promise<Planet> {
        try {
            const planet = await this.bescoveryRepository.getPlanet(id);
            return planet;
        } catch (e) {
            throw new Error(e)
        }
    }

    async fetchBescoveriesFromPlanet(planetId: string): Promise<BescoveryCharacteristics[]> {
        try {
            const p = await this.bescoveryRepository.getAllBescoveriesFromPlanet(planetId)
            const bs = p.map((rawBs: any) => new Bescovery(
                rawBs._id || rawBs.id,
                rawBs.title,
                rawBs.planet,
                rawBs.pilots,
                rawBs.type,
                rawBs.difficulty,
                rawBs.departure_date || rawBs.departureDate,
                rawBs.video_url,
                rawBs.presentation,
                [],
                rawBs.summary
            ))
            return bs.map((bs: Bescovery) => bs.characteristics());
        } catch (e) {
            throw new Error(e);
        }
    }

    async fetchBescoveryFromPlanet(planetId: string, bescoveryId: string): Promise<BescoveryDTO> {
        try {
            let bs = await this.bescoveryRepository.getOneBescoveryFromPlanet(planetId, bescoveryId);
            return bs.full();
        } catch (e) {
            throw new Error(e);
        }
    }

    addBescoveryToPlanet(planetId: string, bescovery: object) {
        return this.bescoveryRepository.getPlanet(planetId)
            .then((planet: any) => {
                return planet && this.bescoveryRepository.createBescovery({
                    ...bescovery,
                    planet: planetId
                })
            })
    }

    async updateBescoveryFromPlanet(planetId: string, bescoveryId: string, bescovery: object): Promise<BescoveryDTO> {
        try {
            const bs = await this.bescoveryRepository.updateBescovery(planetId, bescoveryId, bescovery);
            return bs.full();
        } catch (e) {
            throw new Error(e);
        }
    }

    async deleteBescoveryFromPlanet(planetId: string, bsId: string) : Promise<BescoveryDTO> {
        try {
            let bs = await this.bescoveryRepository.deleteBescoveryFromPlanet(planetId, bsId);
            return bs.full()
        } catch (e) {
            throw new Error(e);
        }
    }

    async isAuthorize(email: string) : Promise<boolean> {
        return await this.bescoveryRepository.isAuthorize(new User(email))
    }
}

export default BeSchool
