
import InMemoryBescoveryRepository from "../TemporaryBescoveryRepository";
import server from "../../src/server";
const chai = require('chai');
const chaiHttp = require('chai-http');
const b = require('../test_bescoveries');

chai.use(chaiHttp);
const expect = chai.expect;

describe('web-api', () => {
    describe('GET: planets/id/bescoveries', () => {
        describe('given there is no bescoveries for the planet Craftsmanship', () => {
            const repository = new InMemoryBescoveryRepository(
                [b.animerUneRetro, b.inventezVotreRetro],
                [b.agility, b.craftsmanship]);

            describe('when listing all bescoveries for the planet Craftsmanship', () => {
                const request = chai.request(server(repository))
                    .get('/planets/2/bescoveries');

                it('should return an empty json array', (done) => {
                    request.end((error: any, response: any) => {
                        expect(error).to.be.null;
                        expect(response).to.be.not.null;
                        expect(response).to.have.status(200);
                        expect(response).to.be.json;
                        const bescoveries = response.body;
                        expect(bescoveries).to.be.an('array');
                        expect(bescoveries).to.be.empty;
                        done();
                    });
                });
            });
        });
        describe('given there is 2 bescoveries for the planet agility and 1 for the planet Craft', () => {
            const repository = new InMemoryBescoveryRepository(
                [b.animerUneRetro, b.inventezVotreRetro, b.dojoCraft],
                [b.agility, b.craftsmanship]);

            describe('when listing all bescoveries for the planet agility', () => {

                const request = chai.request(server(repository))
                    .get('/planets/1/bescoveries');

                it('should return a json array containing the previews of the 2 agility bescoveries', (done) => {
                    request.end((error: any, response: any) => {
                        expect(error).to.be.null;
                        expect(response).to.be.not.null;
                        expect(response).to.have.status(200);
                        expect(response).to.be.json;
                        const bescoveries = response.body;
                        expect(bescoveries).to.be.an('array');
                        expect(bescoveries).to.have.length(2);
                        expect(bescoveries).to.deep.include({
                            id: 0,
                            title: 'Animer une rétro comme sur le terrain',
                            pilots: ["Alex Q."],
                            type: 0,
                            difficulty: 3,
                            departure_date: "2018-01-12"
                        });
                        expect(bescoveries).to.deep.include({
                            id: 1,
                            title: 'Inventez votre rétro',
                            pilots: ["A. Quach"],
                            type: 1,
                            difficulty: 2,
                            departure_date: "2018-01-18"
                        });
                        done();
                    });
                });
            });
            describe('when listing all bescoveries for the planet Craftsmanship', () => {
                const request = chai.request(server(repository))
                    .get('/planets/2/bescoveries');

                it('should return a json array containing the previews of the Craftsmanship bescovery', (done) => {
                    request.end((error: any, response: any) => {
                        expect(error).to.be.null;
                        expect(response).to.be.not.null;
                        expect(response).to.have.status(200);
                        expect(response).to.be.json;
                        const bescoveries = response.body;
                        expect(bescoveries).to.be.an('array');
                        expect(bescoveries).to.have.length(1);
                        expect(bescoveries).to.deep.include({
                            id: 2,
                            title: 'Dojo Craft',
                            pilots: ["Thomas B."],
                            type: 2,
                            difficulty: 1,
                            departure_date: "2018-05-15"
                        });
                        done();
                    });
                });
            });
        });
    });
});
