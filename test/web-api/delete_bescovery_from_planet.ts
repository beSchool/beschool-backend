'use strict';

import server from "../../src/server";
import InMemoryBescoveryRepository from "../TemporaryBescoveryRepository";
import chai = require("chai");
import chaiHttp = require("chai-http");
import b = require("../test_bescoveries");

chai.use(chaiHttp);
const expect = chai.expect;

describe('web-api', () => {
    describe('DELETE: planets/:id/bescoveries/:bsId', () => {
        describe('given there is an existing planet for the planetID', () => {
            const repository = new InMemoryBescoveryRepository(
                [b.animerUneRetro, b.inventezVotreRetro],
                [b.agility, b.craftsmanship]);

            describe('when deleting a bescovery to the planet Craftsmanship', () => {
                const request = chai.request(server(repository))
                    .del('/planets/1/bescoveries/1');

                it('should return a 200 no content status', (done) => {
                    request.end((error, response) => {
                        expect(error).to.be.null;
                        expect(response).to.be.not.null;
                        expect(response).to.have.status(200);
                        expect(repository.getAllBescoveries().length).to.equal(1);
                        expect(response).to.be.json;
                        const bescoveries = response.body;
                        expect(bescoveries).to.be.an('object');
                        expect(bescoveries).to.deep.include({
                            id: 1,
                            title: 'Inventez votre rétro',
                            planet: 'Agility',
                            pilots: ["A. Quach"],
                            type: 1,
                            difficulty: 2,
                            departure_date: "2018-01-18"
                        });
                        done();
                    });
                });
            });
        });
    });
});
