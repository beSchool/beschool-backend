'use strict';

import InMemoryBescoveryRepository from "../TemporaryBescoveryRepository";
import server from "../../src/server";
import chai = require("chai");
import chaiHttp = require("chai-http");
import b = require("../test_bescoveries");

chai.use(chaiHttp);
const expect = chai.expect;

describe('web-api', () => {
    describe('GET: /planets', () => {
        describe('given the repository is empty', () => {
            const repository = new InMemoryBescoveryRepository([], []);
            describe('when listing all the planets', () => {
                const request = chai.request(server(repository))
                    .get('/planets');

                it('should return an empty json array', (done) => {
                    request.end((error, response) => {
                        expect(error).to.be.null;
                        expect(response).to.be.not.null;
                        expect(response).to.have.status(200);
                        expect(response).to.be.json;
                        const planets = response.body;
                        expect(planets).to.be.an('array');
                        expect(planets).to.be.empty;
                        done()
                    })
                })
            })
        });
        describe('given there is 2 planets in the repository', () => {
            const repository = new InMemoryBescoveryRepository(
                [],
                [b.agility, b.craftsmanship]);

            describe('when listing all the planets', () => {
                const request = chai.request(server(repository))
                    .get('/planets');

                it('should return a json array containing 2 planets', (done) => {
                    request.end((error, response) => {
                        expect(error).to.be.null;
                        expect(response).to.be.not.null;
                        expect(response).to.have.status(200);
                        expect(response).to.be.json;
                        const planets = response.body;
                        expect(planets).to.be.an('array');
                        expect(planets).to.have.length(2);
                        expect(planets).to.deep.include({id: 1, name: 'Agility'});
                        expect(planets).to.deep.include({id: 2, name: 'Craftsmanship'});
                        done()
                    })
                })
            })
        })
    })
});
