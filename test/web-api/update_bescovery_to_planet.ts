'use strict';

import server from "../../src/server";
import InMemoryBescoveryRepository from "../TemporaryBescoveryRepository";
import chai = require("chai");
import chaiHttp = require("chai-http");
import b = require("../test_bescoveries");

chai.use(chaiHttp);
const expect = chai.expect;

describe('web-api', () => {
    describe('PUT: planets/:id/bescoveries/:bsId', () => {
        describe('given there is an existing planet for the planetID', () => {
            const repository = new InMemoryBescoveryRepository(
                [b.animerUneRetro, b.inventezVotreRetro],
                [b.agility, b.craftsmanship]);

            describe('when updating a bescovery to the planet Craftsmanship', () => {
                const request = chai.request(server(repository))
                    .put('/planets/2/bescoveries/0')
                    .send({ id: "0", title: 'the best title' });

                it('should return a 200 status and updated title', (done) => {
                    request.end((error, response) => {
                        expect(error).to.be.null;
                        expect(response).to.have.status(200);
                        expect(response).to.be.json;
                        const bescoveries = response.body;
                        expect(bescoveries).to.be.an('object');
                        expect(bescoveries).to.deep.include({
                            title: 'the best title'
                        });
                        done();
                    });
                });
            });
        });
    });
});
