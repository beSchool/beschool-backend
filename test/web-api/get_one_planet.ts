'use strict';

import InMemoryBescoveryRepository from "../TemporaryBescoveryRepository";
import server from "../../src/server";
import chai = require("chai");
import chaiHttp = require("chai-http");
import b = require("../test_bescoveries");

chai.use(chaiHttp);
const expect = chai.expect;

describe('web-api', () => {
    describe('GET: /planets/id', () => {
        describe('given the repository is empty', () => {
            const repository = new InMemoryBescoveryRepository([], []);
            describe('when getting a planet', () => {
                const request = chai.request(server(repository))
                    .get('/planets/1');

                it('should return a 422 response', (done) => {
                    request.end((error, response) => {
                        expect(error).to.be.null;
                        expect(response).to.be.not.null;
                        expect(response).to.have.status(422);
                        done()
                    })
                })
            })
        });
        describe('given there is a planet with id 2 in the repository', () => {
            const repository = new InMemoryBescoveryRepository(
                [],
                [b.craftsmanship]);

            describe('when getting the planet with id of 2', () => {
                const request = chai.request(server(repository))
                    .get('/planets/2');

                it('should return a json array containing 1 planets', (done) => {
                    request.end((error, response) => {
                        expect(error).to.be.null;
                        expect(response).to.be.not.null;
                        expect(response).to.have.status(200);
                        expect(response).to.be.json;
                        const planets = response.body;
                        expect(planets).to.be.an('object');
                        expect(planets).to.contain({id: 2, name: 'Craftsmanship'});
                        done()
                    })
                })
            })
        })
    })
});
