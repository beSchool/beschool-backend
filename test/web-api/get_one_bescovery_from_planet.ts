'use strict';

import InMemoryBescoveryRepository from "../TemporaryBescoveryRepository";
import server from "../../src/server";
import chai = require('chai')
import chaiHttp = require('chai-http')
import b = require('../test_bescoveries')
import { request } from 'https';

chai.use(chaiHttp);
const expect = chai.expect;

describe('web-api', () => {
    describe('GET: planets/id/bescoveries/bsID', () => {
        describe('given there is no bescovery', () => {
            const repository = new InMemoryBescoveryRepository(
                [],
                [b.agility, b.craftsmanship]);

            describe('when trying to get a bescovery', () => {
                const request = chai.request(server(repository))
                    .get('/planets/2/bescoveries/1');

                it('should return a 422 http error', (done) => {
                    request.end((error, response) => {
                        expect(error).to.be.null;
                        expect(response).to.be.not.null;
                        expect(response).to.have.status(422);
                        done();
                    });
                });
            });
        });
        describe('given there is one bescovery belonging to the planet Craft', () => {
            const repository = new InMemoryBescoveryRepository(
                [b.animerUneRetro, b.inventezVotreRetro, b.dojoCraft],
                [b.agility, b.craftsmanship]);

            describe('when getting that bescovery', () => {
                const request = chai.request(server(repository))
                    .get('/planets/1/bescoveries/1');

                it('should return the asked bescovery', (done) => {
                    request.end((error, response) => {
                        expect(error).to.be.null;
                        expect(response).to.be.not.null;
                        expect(response).to.have.status(200);
                        expect(response).to.be.json;
                        const bescoveries = response.body;
                        expect(bescoveries).to.be.an('object');
                        expect(bescoveries).to.deep.include({
                            id: 1,
                            title: 'Inventez votre rétro',
                            planet: 'Agility',
                            pilots: ["A. Quach"],
                            type: 1,
                            difficulty: 2,
                            departure_date: "2018-01-18"
                        });
                        expect(bescoveries).to.not.have.property("video");
                        expect(bescoveries).to.not.have.property("presentation");
                        done();
                    });
                });
            });


            describe('when getting that bescovery but on the wrong planet', () => {
                const request = chai.request(server(repository))
                    .get('/planets/2/bescoveries/1');

                it('should return a 422 error code', (done) => {
                    request.end((error, response) => {
                        expect(error).to.be.null;
                        expect(response).to.be.not.null;
                        expect(response).to.have.status(422);
                        done();
                    });
                });
            });
        });
        describe('given one bescovery with a video belonging to the Agility planet', () => {
            const repository = new InMemoryBescoveryRepository(
                [b.animerUneRetro, b.inventezVotreRetro, b.dojoCraft, b.cuisinerDesMugCakes],
                [b.agility, b.craftsmanship]);

            describe('when getting that bescovery', () => {
                const request = chai.request(server(repository))
                    .get('/planets/1/bescoveries/3');

                it('should return an object with a video_url field', (done) => {
                    request.end((error, response) => {
                        expect(error).to.be.null;
                        expect(response).to.be.not.null;
                        expect(response).to.have.status(200);
                        expect(response).to.be.json;
                        const bescoveries = response.body;
                        expect(bescoveries).to.be.an('object');
                        expect(bescoveries).to.deep.include({
                            id: 3,
                            title: 'Cuisiner des Mug-Cakes',
                            planet: 'Agility',
                            pilots: ["Chloé Grandin"],
                            type: 4,
                            difficulty: 2,
                            departure_date: "2018-05-23",
                            video: {
                                url: "https://www.youtube.com/watch?v=dQw4w9WgXcQ",
                                preview_images: {
                                    high_res: "https://img.youtube.com/vi/dQw4w9WgXcQ/hqdefault.jpg",
                                    medium_res: "https://img.youtube.com/vi/dQw4w9WgXcQ/mqdefault.jpg",
                                    low_res: "https://img.youtube.com/vi/dQw4w9WgXcQ/default.jpg"
                                }
                            }
                        });
                        done();
                    });
                });
            });
        });
        describe('Given one bescovery has a presentation and a video', () => {
            const bescoRepo = new InMemoryBescoveryRepository([b.cuisinerDesMugCakesWithPresentationAndVideo], [b.agility]);

            describe('When getting that bescovery details', () => {
                const request = chai.request(server(bescoRepo))
                    .get('/planets/1/bescoveries/4');

                it('Should return an object with presentation and video field', (done) => {
                    request.end((error, response) => {
                        expect(error).to.be.null;
                        expect(response).to.be.not.null;
                        expect(response).to.have.status(200);
                        expect(response).to.be.json;
                        const bescoveries = response.body;
                        expect(bescoveries).to.be.an('object');
                        expect(bescoveries).to.deep.include({
                            presentation: "/uneurl/john_le_clodo.sdf",
                            video: {
                                url: "https://www.youtube.com/watch?v=dQw4w9WgXcQ",
                                preview_images: {
                                    high_res: "https://img.youtube.com/vi/dQw4w9WgXcQ/hqdefault.jpg",
                                    medium_res: "https://img.youtube.com/vi/dQw4w9WgXcQ/mqdefault.jpg",
                                    low_res: "https://img.youtube.com/vi/dQw4w9WgXcQ/default.jpg"
                                }
                            }
                        });
                        done();
                    });
                });
            });
        });
        describe('Given one bescovery has a presentation but no video', () => {
            const bescoRepo = new InMemoryBescoveryRepository([b.cuisinerDesMugCakesWithoutVideoWithPres], [b.agility]);

            describe('When getting that bescovery details', () => {
                const request = chai.request(server(bescoRepo))
                    .get('/planets/1/bescoveries/5');

                it('Should return an object with presentation field', (done) => {
                    request.end((error, response) => {
                        expect(error).to.be.null;
                        expect(response).to.be.not.null;
                        expect(response).to.have.status(200);
                        expect(response).to.be.json;
                        const bescoveries = response.body;
                        expect(bescoveries).to.be.an('object');
                        expect(bescoveries).to.deep.include({
                            presentation: "/uneurl/john_le_clodo.sdf"
                        });
                        expect(bescoveries).to.not.have.property("video");
                        done();
                    });
                });
            })
        })
        describe('Given one bescovery has images', () => {
            const bescoRepo = new InMemoryBescoveryRepository([b.bescoveryWithImages], [b.agility]);

            describe('When getting that bescovery details', () => {
                const request = chai.request(server(bescoRepo))
                    .get('/planets/1/bescoveries/6');

                it('Should return an object with a gallery field', (done) => {
                    request.end((error, response) => {
                        expect(error).to.be.null;
                        expect(response).to.be.not.null;
                        expect(response).to.have.status(200);
                        expect(response).to.be.json;
                        const bescoveries = response.body;
                        expect(bescoveries).to.be.an('object');
                        expect(bescoveries).to.deep.include({
                            gallery: [
                                {
                                    high_res: "/une/url",
                                    medium_res: "/une/url"
                                },
                                {
                                    high_res: "/une/autre/url",
                                    medium_res: "/une/autre/url"
                                }
                            ]
                        });
                        done();
                    });
                });
            })
        })
        describe('Given one bescovery has a summary', () => {
            const bescoRepo = new InMemoryBescoveryRepository([b.biscoWithSummary], [b.agility]);
            describe('When getting that bescovery summary', () => {
                const request = chai.request(server(bescoRepo))
                    .get('/planets/1/bescoveries/7');
                it('Should return an object with a summuray', (done) => {
                    request.end((error, response) => {
                        expect(error).to.be.null;
                        expect(response).to.be.not.null;
                        expect(response).to.have.status(200);
                        expect(response).to.be.json;
                        const bescoveries = response.body;
                        expect(bescoveries).to.be.an('object');
                        expect(bescoveries).to.deep.include({
                            summary: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis eleifend fringilla sem nec dapibus. Praesent ac cursus ligula, eu malesuada felis. Aenean lorem massa, egestas id consequat euismod, posuere at ligula. Nunc hendrerit lectus quam, sit amet molestie ipsum gravida id. Nulla dictum elit lacus, ac scelerisque purus venenatis eu. Donec at lacus vitae nunc malesuada mollis. Proin sollicitudin rhoncus nunc, at aliquam massa cursus vel. Sed vitae tempor orci. Aenean sapien nunc, viverra eu fringilla vitae, placerat in tortor. In dignissim justo elit, nec pellentesque ante faucibus et. Pellentesque ut scelerisque neque, eget aliquam nibh. Donec sagittis ex ut odio molestie maximus. Suspendisse eget quam vitae urna volutpat eleifend sit amet id massa. Nam pharetra ante ac sapien auctor lobortis. Mauris in varius diam, sed porttitor el Integer sit amet pulvinar libero, vel vulputate nibh. Nunc sed ornare sem, euismod mattis sapien. Fusce quis ipsum sit amet dui rhoncus facilisis eu finibus odio. In est sapien, vestibulum vel tincidunt eget, dictum at magna. In a fermentum felis. Morbi quis orci nunc. Maecenas vulputate, velit at auctor sagittis, ante eros tincidunt urna, in maximus mi metus sit amet magna. Aenean interdum erat quis turpis elementum rhoncus. Integer ullamcorper tincidunt ex, at vestibulum felis semper eu. Maecenas orci erat, vestibulum at mattis id, dapibus ac massa. Nunc sollicitudin augue in accumsan scelerisque. Sed vulputate varius nibh ut ultrices. Integer est erat, lacinia vel mattis id, pellentesque id lorem. Vestibulum placerat aliquam lacus, sed lobortis libero. Praesent non nunc ut augue iaculis euismod. Sed gravida est imperdiet metus pellentesque, nec vestibulum magna egestas."
                        });
                        done();
                    });
                })
            });
        });
    });
});