'use strict';

import server from "../../src/server";
import InMemoryBescoveryRepository from "../TemporaryBescoveryRepository";
import chai = require("chai");
import chaiHttp = require("chai-http");
import b = require("../test_bescoveries");

chai.use(chaiHttp);
const expect = chai.expect;

describe('web-api', () => {
    describe('POST: planets/:id/bescoveries', () => {
        describe('given there is no planets for the planetID', () => {
            const repository = new InMemoryBescoveryRepository(
                [b.animerUneRetro, b.inventezVotreRetro],
                [b.agility, b.craftsmanship]);

            describe('when adding a bescovery to the planet XXX', () => {
                const request = chai.request(server(repository))
                    .post('/planets/42/bescoveries')
                    .send({ title: 'the title' });

                it('should return a 422 error', (done) => {
                    request.end((error, response) => {
                        expect(error).to.be.null;
                        expect(response).to.be.not.null;
                        expect(response).to.have.status(422);
                        done();
                    });
                });
            });
        });
        describe('given there is an existing planet for the planetID', () => {
            const repository = new InMemoryBescoveryRepository(
                [b.animerUneRetro, b.inventezVotreRetro],
                [b.agility, b.craftsmanship]);

            describe('when adding a bescovery to the planet Craftsmanship', () => {
                const request = chai.request(server(repository))
                    .post('/planets/2/bescoveries')
                    .send({ title: 'the best title' });

                it('should return a 204 no content status', (done) => {
                    request.end((error, response) => {
                        expect(error).to.be.null;
                        expect(response).to.be.not.null;
                        expect(response).to.have.status(204);
                        expect(repository.getAllBescoveries().length).to.equal(3);
                        done();
                    });
                });
            });
        });
    });
});
