'use strict';

import server from "../../src/server";
import InMemoryImageRepository from "../InMemoryImageRepository";
import InMemoryBescoveryRepository from "../TemporaryBescoveryRepository";
import chai = require("chai");
import chaiHttp = require("chai-http");
import b = require("../test_bescoveries");

chai.use(chaiHttp);
const expect = chai.expect;

describe('web-api', () => {
    describe('POST: upload/ a new file', () => {
        const bescoveryRepository = new InMemoryBescoveryRepository(
            [b.animerUneRetro, b.inventezVotreRetro],
            [b.agility, b.craftsmanship]);
        const imageRepository = new InMemoryImageRepository();

        it('Should return a 200 http status code', (done) => {
            let file = {
                fileName: "devJoke.png",
            }

            chai.request(server(bescoveryRepository, imageRepository))
                .post('/upload')
                .set('Accept', 'application/json')
                .set('Content-Type', 'multipart/form-data')
                .attach('file', "./images/space.jpg")
                .end((error, response) => {
                    expect(error).to.be.null;
                    expect(response).to.be.not.null;
                    expect(response).to.have.status(200);
                    done()
                })
        })
    });
});
