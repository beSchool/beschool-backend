'use strict';

import server from "../../src/server";
import InMemoryBescoveryRepository from "../TemporaryBescoveryRepository";
import chai = require("chai");
import chaiHttp = require("chai-http");
import {hugo} from "../test_users";

chai.use(chaiHttp);
const expect = chai.expect;

describe('web-api', () => {
    describe('POST: /admin/', () => {
        describe('given there is no user with this email address', () => {
            const repository = new InMemoryBescoveryRepository(
                [], [], []);

            describe('when asking if the user is authorize', () => {
                const request = chai.request(server(repository))
                    .post('/admin/')
                    .send({ email: 'hugo.courtecuisse@benextcompany.com' });

                it('should return a 200 and is_authorize to false', (done) => {
                    request.end((error, response) => {
                        expect(error).to.be.null;
                        expect(response).to.be.not.null;
                        expect(response).to.have.status(200);
                        expect(response).to.be.json;
                        const json = response.body;
                        expect(json).to.be.an('object');
                        expect(json).to.contain({ is_authorize: false });
                        done();
                    });
                });
            });
        });
        describe('given there is a user with this email address', () => {
            const repository = new InMemoryBescoveryRepository(
                [], [], [hugo]);

            describe('when asking if the user is authorize', () => {
                const request = chai.request(server(repository))
                    .post('/admin/')
                    .send({ email: 'hugo.courtecuisse@benextcompany.com' });

                it('should return a 200 and is_authorize to true', (done) => {
                    request.end((error, response) => {
                        expect(error).to.be.null;
                        expect(response).to.be.not.null;
                        expect(response).to.have.status(200);
                        expect(response).to.be.json;
                        const json = response.body;
                        expect(json).to.be.an('object');
                        expect(json).to.contain({ is_authorize: true });
                        done();
                    });
                });
            });
        });
    });
});
