'use strict';

import server from "../../src/server";
import InMemoryBescoveryRepository from "../TemporaryBescoveryRepository";
import chai = require("chai");
import chaiHttp = require("chai-http");

chai.use(chaiHttp);
const expect = chai.expect;

describe('web-api', () => {
    describe('GET: on an unknown url', () => {
        it('should return a 404 error code', (done) => {
            chai.request(server(new InMemoryBescoveryRepository([], [])))
                .get('/unknown-url')
                .end((error, response) => {
                    expect(error).to.be.null;
                    expect(response).to.be.not.null;
                    expect(response).to.have.status(404);
                    done()
                })
        })
    })
});
