'use strict';

import BescoveryRepository from "../src/repositories/BescoveryRepository";
import Bescovery from "../src/beschool/bescovery/Bescovery";
import Planet from "../src/beschool/planet/Planet";
import User from "../src/beschool/bescovery/User";

class TemporaryBescoveryRepository implements BescoveryRepository {
    private bescoveries: Bescovery[];
    private planets: Planet[];
    private readonly users: User[];

    constructor(bescoveries: Bescovery[], planets: Planet[], users: User[] = []) {
        this.bescoveries = bescoveries;
        this.planets = planets;
        this.users = users;
    }

    getAllBescoveries(): Bescovery[] {
        return this.bescoveries
    }

    getAllBescoveriesFromPlanet(planetId: string): Promise<Bescovery[]> {
        return new Promise((resolve, reject) => {
            const bs = this.bescoveries.filter(b => b.isFrom(planetId))
            if (!bs) reject(new Error(`No bescoveries for Planet ${planetId}`))
            resolve(bs)
        })
    }

    getOneBescoveryFromPlanet(planetId: string, bescoveryId: string): Promise<Bescovery> {
        return new Promise((resolve, reject) => {
            const bs = this.bescoveries.filter(b => b.isFrom(planetId) && b.hasId(bescoveryId))
            if (!bs || !bs.length) reject(new Error('BS not found'))
            resolve(bs[0])
        })
    }

    getAllPlanets(): Promise<Planet[]> {
        return Promise.resolve(this.planets)
    }

    getPlanet(id: string): Promise<Planet> {
        return new Promise((resolve, reject) => {
            const planet = this.planets.filter(p => p.id === parseInt(id));
            if (!planet || !planet.length) return reject(new Error(`Planet ${id} not found`))
            resolve(planet[0])
        })
    }

    createPlanet(name: string): void {}
    createBescovery(data: { id: string, title: string }):Promise<any> {
        return new Promise((resolve, reject) => {
            const bs = new Bescovery(
                +data.id || 0,
                data.title || 'title',
                new Planet(0, 'ok'),
                [],
                0,
                0,
                ''
            )
            this.bescoveries.push(bs)
            resolve()
        })
    }

    addNewFile(protocol: string, host: string, uploadFolder: string, newFileName: string): Promise<any> {
        return new Promise((resolve, reject) => {
            const fileUrl = `${protocol}://${host}/${uploadFolder}/${newFileName}`;
            resolve(fileUrl)
        })
    }

    updateBescovery(planetId: string, bescoveryId: string, data: { id: string, title: string }): Promise<any> {
        return new Promise((resolve, reject) => {
            this.bescoveries.map(function (oldBs) {
                if (oldBs.hasId(bescoveryId)) {
                    resolve (new Bescovery(
                        +data.id || 0,
                        data.title || 'title',
                        new Planet(0, 'ok'),
                        [],
                        0,
                        0,
                        ''))
                } else {
                    oldBs
                }
            })
            resolve()
        })
    }

    deleteBescoveryFromPlanet(planetId: string, bsId: string): Promise<any> {
        return new Promise((resolve, reject) => {
            const index = this.bescoveries.findIndex(e => e.hasId(bsId))

            if (index != -1) {
                const bs = this.bescoveries[index]
                this.bescoveries.splice(index, 1)
                resolve(bs)
            }
        })
    }

    isAuthorize(user: User): Promise<boolean> {
        return new Promise((resolve, reject) => {
            const foundUser = this.users.filter(currentUser => {
                return currentUser.email === user.email
            })

            if (!foundUser || foundUser.length == 0) {
                resolve(false)
            }

            resolve(true)
        })
    }
}

export default TemporaryBescoveryRepository
