'use strict';

import InMemoryBescoveryRepository from "../TemporaryBescoveryRepository";
import server from "../../src/server";
import chai = require("chai");
import chaiHttp = require("chai-http");
import b = require("../test_bescoveries");

chai.use(chaiHttp);
const expect = chai.expect;

describe('integration', () => {
  describe('Given there is a presentation available', () => {
    describe('When fetching that presentation', () => {
      const request = chai.request(server(new InMemoryBescoveryRepository([], []), undefined, './test/integration/files/'))
        .get('/presentation.txt');
      it('Then it should return 200 error code', (done) => {
        request.end((error, response) => {
          expect(error).to.be.null;
          expect(response).to.be.not.null;
          expect(response).to.be.text;
          expect(response).to.have.status(200);
          done();
        });
      })
    })
  })
});
