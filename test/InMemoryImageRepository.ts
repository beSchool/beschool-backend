import ImageRepository from "../src/repositories/ImageRepository";

const multer = require("multer");

class InMemoryImageRepository implements ImageRepository {
    private storage: any;
    private fileFilter: any;
    private fileSizeLimit: number;

    constructor(/*uploadPath: String*/) {
        this.storage = this.getStorage('qsdqsd')
        this.fileFilter = this.getFileFilter()
        this.fileSizeLimit = 4 * 1024 * 1024
    }

    getUploadPath(): String {
        return ''
    }

    getStorage(uploadPath: String): any {
        return multer.memoryStorage(/*{
            destination,
            filename
        }*/)
    }

    getFileFilter(): any {
        return {}
    }

    getUploadMiddleware(): any {
        return multer({ storage: this.storage })
    }
}

export default InMemoryImageRepository;