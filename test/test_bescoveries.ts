'use strict';

import Bescovery from "../src/beschool/bescovery/Bescovery";
import Planet from "../src/beschool/planet/Planet";

export const agility = new Planet(1, 'Agility');
export const craftsmanship = new Planet(2, 'Craftsmanship');
export const animerUneRetro = new Bescovery(0, 'Animer une rétro comme sur le terrain', agility, ["Alex Q."], 0, 3, "2018-01-12");
export const inventezVotreRetro = new Bescovery(1, 'Inventez votre rétro', agility, ["A. Quach"], 1, 2, "2018-01-18");
export const dojoCraft = new Bescovery(2, 'Dojo Craft', craftsmanship, ["Thomas B."], 2, 1, "2018-05-15");
export const cuisinerDesMugCakes = new Bescovery(3, 'Cuisiner des Mug-Cakes', agility, ["Chloé Grandin"], 4, 2, "2018-05-23", "https://www.youtube.com/watch?v=dQw4w9WgXcQ");

export const cuisinerDesMugCakesWithPresentationAndVideo = new Bescovery(
    4,
    'Cuisiner des Mug-Cakes avec présentation',
    agility,
    ["Chloé Grandin"],
    4,
    2,
    "2018-05-23",
    "https://www.youtube.com/watch?v=dQw4w9WgXcQ",
    "/uneurl/john_le_clodo.sdf"
);

export const cuisinerDesMugCakesWithoutVideoWithPres = new Bescovery(
    5,
    'Cuisiner des Mug-Cakes avec présentation mais sans video',
    agility,
    ["Chloé Grandin"],
    4,
    2,
    "2018-05-23",
    undefined,
    "/uneurl/john_le_clodo.sdf"
);

export const bescoveryWithImages = new Bescovery(
    6,
    'Bescovery with some images',
    agility,
    ["Chloé Gourdin"],
    4,
    2,
    "2018-05-23",
    undefined,
    undefined,
    [
        "/une/url",
        "/une/autre/url"
    ]
);

export const biscoWithSummary = new Bescovery(
    7,
    'Bescovery with the summary',
    agility,
    ["Chloé Gourdin"],
    4,
    2,
    "2018-05-23",
    undefined,
    undefined,
    undefined,
    "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis eleifend fringilla sem nec dapibus. Praesent ac cursus ligula, eu malesuada felis. Aenean lorem massa, egestas id consequat euismod, posuere at ligula. Nunc hendrerit lectus quam, sit amet molestie ipsum gravida id. Nulla dictum elit lacus, ac scelerisque purus venenatis eu. Donec at lacus vitae nunc malesuada mollis. Proin sollicitudin rhoncus nunc, at aliquam massa cursus vel. Sed vitae tempor orci. Aenean sapien nunc, viverra eu fringilla vitae, placerat in tortor. In dignissim justo elit, nec pellentesque ante faucibus et. Pellentesque ut scelerisque neque, eget aliquam nibh. Donec sagittis ex ut odio molestie maximus. Suspendisse eget quam vitae urna volutpat eleifend sit amet id massa. Nam pharetra ante ac sapien auctor lobortis. Mauris in varius diam, sed porttitor el Integer sit amet pulvinar libero, vel vulputate nibh. Nunc sed ornare sem, euismod mattis sapien. Fusce quis ipsum sit amet dui rhoncus facilisis eu finibus odio. In est sapien, vestibulum vel tincidunt eget, dictum at magna. In a fermentum felis. Morbi quis orci nunc. Maecenas vulputate, velit at auctor sagittis, ante eros tincidunt urna, in maximus mi metus sit amet magna. Aenean interdum erat quis turpis elementum rhoncus. Integer ullamcorper tincidunt ex, at vestibulum felis semper eu. Maecenas orci erat, vestibulum at mattis id, dapibus ac massa. Nunc sollicitudin augue in accumsan scelerisque. Sed vulputate varius nibh ut ultrices. Integer est erat, lacinia vel mattis id, pellentesque id lorem. Vestibulum placerat aliquam lacus, sed lobortis libero. Praesent non nunc ut augue iaculis euismod. Sed gravida est imperdiet metus pellentesque, nec vestibulum magna egestas."
);