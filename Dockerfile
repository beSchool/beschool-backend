FROM node:carbon

WORKDIR /srv/app

COPY docker-entrypoint.sh /entrypoint.sh

RUN chown root:root /entrypoint.sh && chmod a+x /entrypoint.sh \
    && npm install -g api-console-cli@0.2.12 typescript

ENTRYPOINT ["/entrypoint.sh"]

CMD npm run start
