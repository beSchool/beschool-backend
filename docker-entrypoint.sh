#!/bin/bash

if [ ! -d "./build" ]; then
    api-console build doc/index.raml && api-console serve --hostname 0.0.0.0 -p 8000 build &
else
    api-console serve --hostname 0.0.0.0 -p 8000 build &
fi

exec "$@"
