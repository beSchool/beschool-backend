# Welcome to the beSchool !

![Welcome](./images/space.jpg)

## About

This is the back-end for the beSchool project

## How to fetch needed dependencies

You will need to install the packages for `nodejs` and `npm`.

```
apt-get install nodejs
```

After cloning this repository, type `cd bescovery-web; npm install`.

## How to execute the tests

You can launch the full test suite by running `npm test`.

## Docker Install

If you use Docker, you can easily launch the project with Docker Compose:

```
docker-compose up -d
```

And then you can access the API on ``http://localhost``

## Online Access

You can access on the live server at this adress

``http://murmuring-hamlet-46088.herokuapp.com/planets/2/bescoveries``
